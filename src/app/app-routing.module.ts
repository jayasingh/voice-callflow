import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppletsComponent } from './components/applets/applets.component';
import { CallFlowListComponent } from './components/call-flow-list/call-flow-list.component';


const routes: Routes = [
	{ path: '', pathMatch: 'full', redirectTo: 'call-flows' },
	{ path: 'call-flow-designer', component: AppletsComponent },
	{ path: 'call-flow-designer/:id', component: AppletsComponent },
	{ path: 'call-flows', component: CallFlowListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
