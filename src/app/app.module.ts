import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TitleComponent } from './components/title/title.component';
import { AppletsComponent } from './components/applets/applets.component';
import { GreetingsComponent } from './components/greetings/greetings.component';
import { HangupComponent } from './components/hangup/hangup.component';
import { MenusComponent } from './components/menus/menus.component';
import { CallFlowListComponent } from './components/call-flow-list/call-flow-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AppletsComponent,
    GreetingsComponent,
    CallFlowListComponent,
    HangupComponent,
    MenusComponent,
    TitleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DragDropModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [GreetingsComponent, MenusComponent, HangupComponent]
})
export class AppModule { }
