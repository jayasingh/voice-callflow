import { Component, Input } from "@angular/core";

@Component({
  selector: 'app-title',
  styleUrls: ['./title.component.scss'],
  templateUrl: './title.component.html'
})

export class TitleComponent {
  @Input() title: string;
  @Input() width: number;
  constructor() { }
}