import { Component, OnInit } from '@angular/core';
import { CallFlowService } from "./../../services/call-flow.service";

@Component({
  selector: 'app-call-flow-list',
  templateUrl: './call-flow-list.component.html',
  styleUrls: ['./call-flow-list.component.scss']
})
export class CallFlowListComponent implements OnInit {

	flows: any = [];
  pages = [];
  success = false;
  error = false;
  message = '';
  constructor(public callFlow: CallFlowService) { }

	ngOnInit() {
	  this.loadFlows(1)
	}

  // Get flows list
  loadFlows(page) {
    this.pages=[];
    return this.callFlow.getFlows(page).subscribe((data: {}) => {
      if(data['success']){
        console.log(data);
        this.flows = data['data'];
        var range = 2;
        var start = 1;
        var end = this.flows['last_page'];
        if(this.flows['last_page']>5){
          start = Math.min(Math.max(this.flows['current_page']-range,1),this.flows['last_page']-4);
          end =  Math.max(Math.min(this.flows['current_page']+range,this.flows['last_page']),5);
        }
        for (let i = start; i <= end; i++){
          this.pages.push(i);
        }

      }

    });
    // return this.callFlow.getFlows().subscribe((data: {}) => {
    //   if (data['success']) {
    //     this.flows = data['data']['data'];
    //   }
    // });
  }
  //delete a callFlow
  deleteFlow(flowId) {
    if(confirm("Are you sure to delete flow id="+flowId)) {
      this.callFlow.deleteFlow(flowId).subscribe((data: {}) => {
        if(data['success']){
          this.success = true;
          this.error = false;
        }
        else{
          this.success = false;
          this.error = true;
        }
        this.message = data['message'];
        this.loadFlows(this.flows['current_page']);
      });
    }
  }
}