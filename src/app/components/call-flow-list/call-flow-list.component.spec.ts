import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallFlowListComponent } from './call-flow-list.component';

describe('CallFlowListComponent', () => {
  let component: CallFlowListComponent;
  let fixture: ComponentFixture<CallFlowListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallFlowListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallFlowListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
