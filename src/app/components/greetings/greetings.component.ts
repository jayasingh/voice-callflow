import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';


declare function makeId(length): any;

@Component({
  selector: 'app-greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.scss']
})
export class GreetingsComponent implements OnInit {

  //applet form model
  public greetingsForm: FormGroup;

  @Output() dropEvent = new EventEmitter();
  @Output() submitEvent = new EventEmitter();

  // we will pass in submitted from Applet component
  @Input() submitted: any;
  @Input() identifier: any;
  @Input() patchData: any;
  
  constructor(private _fb: FormBuilder) { }

  ngOnInit() {
    // we will initialize our form here
      this.greetingsForm = this._fb.group({
        Play:this._fb.group({
            Clip: ['', Validators.required],
            SkipTime: ['10'],
            Next: ['']
        })
      });
      if(this.patchData){
        this.greetingsForm.patchValue(this.patchData);
      }
  }

  drop(event){
    //create a unique identifier
    var uniqueId = makeId(25);
    event.unique_id = uniqueId;
    this.greetingsForm.patchValue({Play:{Next:uniqueId}});
    this.dropEvent.emit(event);
  }

  //convenience getter for easy access to form fields
  get form() { return this.greetingsForm.controls; }

  submit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.greetingsForm.invalid) {
        return;
    }
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.greetingsForm.value))
    var data = {};
    data = this.greetingsForm.value;
    data = Object.assign({},data,{identifier:this.identifier});
    this.submitEvent.emit(data);
  }

}
