import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-hangup',
  templateUrl: './hangup.component.html',
  styleUrls: ['./hangup.component.css']
})
export class HangupComponent implements OnInit {

  //applet form model
  public hangupForm: FormGroup;

	@Output() dropEvent = new EventEmitter();
  @Output() submitEvent = new EventEmitter();

  // we will pass in submitted from Applet component
  @Input() submitted: any;
  @Input() identifier: any;
  @Input() patchData: any;
	
  constructor(private _fb: FormBuilder) { }

	ngOnInit() {
    // we will initialize our form here
    this.hangupForm = this._fb.group({
      HangUp:['hangup']
    });
    if(this.patchData){
      this.hangupForm.patchValue(this.patchData);
    }
	}

	drop(event){
		this.dropEvent.emit(event);
	}

  //convenience getter for easy access to form fields
  get form() { return this.hangupForm.controls; }

  submit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.hangupForm.invalid) {
        return;
    }
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.greetingsForm.value))
    var data = {};
    data = this.hangupForm.value;
    data = Object.assign({},data,{identifier:this.identifier});
    this.submitEvent.emit(data);
  }

}
