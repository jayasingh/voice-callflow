import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';

declare function makeId(length): any;

@Component({
	selector: 'app-menus',
	templateUrl: './menus.component.html',
	styleUrls: ['./menus.component.scss']
})
export class MenusComponent implements OnInit {
	//applet form model
	public menusForm: FormGroup;

	@Output() dropEvent = new EventEmitter();
	@Output() submitEvent = new EventEmitter();

	// we will pass in submitted from Applet component
	@Input() submitted: any;
	@Input() identifier: any;
	@Input() patchData: any;

  	constructor(private _fb: FormBuilder) { }

  	ngOnInit() {
  		// we will initialize our form here
    	this.menusForm = this._fb.group({
    		Menus:this._fb.group({
	            Clip: ['', Validators.required],
	        	Keypress: this._fb.array([
	                this.initKeypress(),
	            ]),
	        	RepeatNoKey:['2'],
	        	RepeatNoKeyPrompt:[''],
	        	RepeatInvalidKey:['2'],
	        	RepeatInvalidKeyPrompt:[''],
	        	WaitDuration:['4'],
	        	SubMenuNoKey:[''],
	        	SubMenuInvalidKey:[''],
	        	Next:['']
    		})
      		
    	});

    	if(this.patchData){
	       this.menusForm.patchValue(this.patchData);
	    }
  	}

  	initKeypress() {
        // initialize our keys
        return this._fb.group({
            key: ['', Validators.required],
            value: []
        });
    }

    addKeypress(e) {
  		e.preventDefault();
    	// add address to the list
    	const control = <FormArray>this.form.get('Keypress');
    	control.push(this.initKeypress());

	}

	removeKeypress(i: number) {
		// remove address from the list
		const control = <FormArray>this.form.get('Keypress');
		control.removeAt(i);
	}

	drop(event) {
		//create a unique identifier
		var uniqueId = makeId(25);
		event.unique_id = uniqueId;
		//add this uniqueId to the dropArea
		var eventTarget = event.target.id;
		switch (eventTarget) {
			case 'menuDropArea': {
				this.menusForm.patchValue({ Menus: { Next: uniqueId } });
				break;
			}
			case String(eventTarget.match(/^keypressDropArea.*/)): {
				let itemIndex = eventTarget.substring(17);
				const control = <FormArray>this.form.get('Keypress');
				control.at(itemIndex).patchValue({ value: uniqueId });
				break;
			}
			default: {
				//not to do anything 
				break;
			}
		}

		this.dropEvent.emit(event);
	}

	//convenience getter for easy access to form fields
	get form() { return this.menusForm.controls.Menus; }

	submit() {

		this.submitted = true;
		// stop here if form is invalid
		if (this.menusForm.invalid) {
			//console.log(this.menusForm);
			return;
		}
		//alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.greetingsForm.value))
		var data = {};
		data = this.menusForm.value;
		data = Object.assign({}, data, { identifier: this.identifier });
		this.submitEvent.emit(data);
	}

}
