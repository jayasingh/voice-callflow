
import { Component, OnInit, ComponentFactoryResolver, Type, ViewChild, ViewContainerRef, Directive } from '@angular/core';

import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { JsonMergerService } from './../../services/json-merger.service';
import { CallFlowService } from './../../services/call-flow.service';
import { ComponentDeclarationService } from './../../services/component-declaration.service';


//declare function allowDrop(event): any;
//declare function dragMe(event): any;

declare function dropMe(event,id): any;
declare function makeId(length): any;
declare function createDeleteNode(): any;

@Component({
	selector: 'app-applets',
	templateUrl: './applets.component.html',
	styleUrls: ['./applets.component.scss']
})
export class AppletsComponent implements OnInit {

	public showEditFlow: Boolean;
	//applet form model
	public callFlowForm: FormGroup;
	//view child dirctive
	@ViewChild('container', { static: false, read: ViewContainerRef }) entry: ViewContainerRef;

	//declare droplets varibale here
	droplets : any;
	//flow array
	submitted = false;
	//flow array
	flow = {};
	//raw flow array
	rawFlow = {};
	//flowId for edit.
	flowId;
	//dropped components
	components = [];
	//Expose class so that it can be used in the template
	droppedComponent;
	//droppedItems
	droppedItems = [];
	//success and error
	success = false;
	error = false;
	message = '';
	//class constructor for loading dependencies
	constructor(
		private componentFactoryResolver: ComponentFactoryResolver,
		public jsonMerger: JsonMergerService,
		public callFlow: CallFlowService,
		private _fb: FormBuilder,
		private route: ActivatedRoute,
		private componentDeclaration : ComponentDeclarationService
	) {
		this.showEditFlow = false;
	}
	//call functions to be executed on init
	ngOnInit() {
		//get droplets list from service
		this.droplets = this.componentDeclaration.getDroplets();
		// we will initialize our form here
      	this.callFlowForm = this._fb.group({
      		name:['',Validators.required]
      	}); 
      	this.route.paramMap.subscribe(params => {
		    this.flowId = params.get('id');
		});
		if(this.flowId){
			this.getFlow(this.flowId);
		}

	}
	//drop event handler function
	drop(event) {
		//create a unique identifier
		var uniqueId = event.unique_id ? event.unique_id : 'firstItem';
		//call dropMe function defined in src/assets/js/custom.js
		if (dropMe(event, uniqueId)) {
			//get dropped item index from the droplets array
			var droppedIndex = this.droplets.findIndex(p => p.id == event.dataTransfer.getData("text/html"));
			// Expose class so that it can be used in the template
			this.droppedComponent = this.droplets[droppedIndex].className;
			//add this item to droppedItems array
			this.droppedItems.push({ id: uniqueId, className: this.droppedComponent });
			//call add component method, to render component html
			this.addComponent(this.droppedComponent, uniqueId);
		}
	}
	//add dynamic component handler function
	addComponent(componentClass: Type<any>, identifier, flow='') {
	  	// Create component dynamically inside the ng-template
	    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentClass);
	    //const component = this.container.createComponent(componentFactory);
	    const componentRef = this.entry.createComponent(componentFactory);
	    // Push the component so that we can keep track of which components are created
	    this.components.push(componentRef);
	    //subscribe to dropEvent from the dynamic component
	    componentRef.instance.dropEvent.subscribe(event => {
	        this.drop(event);
	    });
	    //subscribe to submitEvent from the dynamic component
	    componentRef.instance.submitEvent.subscribe(event => {
	        this.createFlow(event);
	    });
	    componentRef.instance.submitted = false;
	    componentRef.instance.identifier = identifier;
	    if(flow){
	    	componentRef.instance.patchData = flow;
	    }
	}
	//remove component handler function
	removeComponent(componentClass: Type<any>, droppedItemId) {
		// Find the component
		const component = this.components.find((component) => component.instance instanceof componentClass);
		const componentIndex = this.components.indexOf(component);

		if (componentIndex !== -1) {
			// Remove component from both view and array
			this.entry.remove(this.entry.indexOf(component));
			this.components.splice(componentIndex, 1);
			//reset droppedItems array values.
			var deletedItemIndex = this.droppedItems.findIndex(x => x.id == droppedItemId);
			this.droppedItems.splice(deletedItemIndex, 1);
		}
	}
	//add all json values togather.
	createFlow(value){
		//push all these values inside an array.
		this.rawFlow[value.identifier] = JSON.parse(JSON.stringify(value));
		//call json flow merger service here.
		var completeFlow = this.jsonMerger.do(this.flow,this.components, value);
		//var completeFlow = {};
		if(Object.keys(completeFlow).length > 0){
			//prepare insert data.
			var insertData = {
				name:this.callFlowForm.value.name,
				user_id:1,
				flow:JSON.stringify(completeFlow),
				raw_flow:JSON.stringify(this.rawFlow)
			};
			if(this.flowId){
				//call update flow Api and pass insertData here.
				this.callFlow.updateFlow(this.flowId, insertData).subscribe((data: {}) => {
					if(data['success']){
		      			this.success = true;
		      			this.error = false;
			      	}
			      	else{
			      		this.success = false;
			      		this.error = true;
			      	}
			      	this.message = data['message'];
			    });
			}
			else{
				//call create flow Api and pass insertData here.
				this.callFlow.createFlow(insertData).subscribe((data: {}) => {
					if(data['success']){
		      			this.success = true;
		      			this.error = false;
			      	}
			      	else{
			      		this.success = false;
			      		this.error = true;
			      	}
			      	this.message = data['message'];
			    });
			}

		}
		else {
			//diplay validation error message to user.
			//console.log("Validation error");
		}

	}
	//convenience getter for easy access to form fields
	get form() { return this.callFlowForm.controls; }
	//save button has been clicked, submit all components instances.
	saveFlow() {
		this.submitted = true;
		// stop here if form is invalid
		if (this.callFlowForm.invalid) {
			console.log('Invalid.');
			return;
		}
		//reset flow value as form submitted again
		this.flow = {};
		this.rawFlow = {};
    	//console.log("Save Clicked");
    	this.components.forEach(obj => {
    		obj.instance.submit();
    	});
    	//console.log(this.dropOrder);
	}
	//get flow data by id
	getFlow(flowId) {
		//call get flow Api.
		this.callFlow.getFlow(flowId).subscribe((data: {}) => {
			//check if sucess true.
	      	if(data['success']){
	     		//add callflowForm patchValue.
	      		this.callFlowForm.patchValue({name:data['data']['name']});
	      		//get raw flow data.
	      		var flow = JSON.parse(data['data']['raw_flow']);
	      		//get keys in this flow
	      		var keys = Object.keys(flow);
	      		//iterate through each key.
	      		for (let i = 0; i < keys.length; i++) {
	      			//get first key of current item.
	      			var key = Object.keys(flow[keys[i]])[0];
	      			//get uniqueId of this iteration.
	      			var uniqueId = flow[keys[i]]['identifier'];
	      			//find keyIndex from the droplets array.
	      			var keyIndex = this.droplets.findIndex(x => x.formGroupName == key);
	      			var nodeCopy = document.getElementById(this.droplets[keyIndex].id).cloneNode(true);
	      			var target = document.getElementById("defaultDropArea");
	      			var deleteNode = createDeleteNode();
	      			//nodeCopy.id=uniqueId;
	      			nodeCopy.appendChild(deleteNode);
	      			target.appendChild(nodeCopy);
	      			//call addComponents function to create components.
	      			this.addComponent(this.droplets[keyIndex].className,uniqueId,flow[keys[i]]);
	      		}
	      	}
	    })
	}
}
