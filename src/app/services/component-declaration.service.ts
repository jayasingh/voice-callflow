import { Injectable } from '@angular/core';

import { GreetingsComponent } from './../components/greetings/greetings.component';
import { MenusComponent } from './../components/menus/menus.component';
import { HangupComponent } from './../components/hangup/hangup.component';

@Injectable({
  	providedIn: 'root'
})
export class ComponentDeclarationService {
	//declare a varibale
    droplets : any;
  	constructor() { }
  	//declare all droplets here and return to caller.
  	getDroplets() {
  		//droplets items list
		this.droplets = [
	    	{id: 'greetings-component', name:'Greetings', className: GreetingsComponent, formGroupName: 'Play', icon: 'greeting'},
	    	{id: 'menus-component', name:'Menus', className: MenusComponent, formGroupName: 'Menus', icon: 'collect'},
	    	{id: 'hangup-component', name:'Hangup', className: HangupComponent, formGroupName: 'HangUp', icon: 'call'}
		];

		return this.droplets;
  	}

}
