import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Flow } from './../interfaces/flow';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class CallFlowService {

	// Define API
  	apiURL = 'http://callflow.localhost.com/api/v1';
  	constructor(private http: HttpClient) { }


	/*========================================
	  CRUD Methods for consuming RESTful API
	=========================================*/

  	// Http Options
  	httpOptions = {
    	headers: new HttpHeaders({
      		'Content-Type': 'application/json'
    	})
  	} 
  	// HttpClient API get() method => Fetch flows list
  	getFlows(page): Observable<Flow> {
    	return this.http.get<Flow>(this.apiURL + '/call-flow?page='+page)
	    .pipe(
	      	retry(1),
	      	catchError(this.handleError)
	    )
  	}
    // HttpClient API get() method => Fetch single flow 
    getFlow(id): Observable<Flow> {
      return this.http.get<Flow>(this.apiURL + '/call-flow/'+id)
      .pipe(
          retry(1),
          catchError(this.handleError)
      )
    }
  	// HttpClient API post() method => Create Flow
  	createFlow(flow): Observable<Flow> {
    	return this.http.post<Flow>(this.apiURL + '/call-flow', JSON.stringify(flow), this.httpOptions)
	    .pipe(
	      	retry(1),
	      	catchError(this.handleError)
	    )
  	}  
  	// HttpClient API put() method => Update employee
  	updateFlow(id, flow): Observable<Flow> {
    	return this.http.put<Flow>(this.apiURL + '/call-flow/' + id, JSON.stringify(flow), this.httpOptions)
	    .pipe(
	      	retry(1),
	      	catchError(this.handleError)
	    )
  	}

  	// HttpClient API delete() method => Delete employee
  	deleteFlow(id){
    	return this.http.delete<Flow>(this.apiURL + '/call-flow/' + id, this.httpOptions)
	    .pipe(
	      	retry(1),
	      	catchError(this.handleError)
	    )
  	}
  	// Error handling 
  	handleError(error) {
     	let errorMessage = '';
     	if(error.error instanceof ErrorEvent) {
       		// Get client-side error
       		errorMessage = error.error.message;
     	} else {
       		// Get server-side error
       		errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     	}
     	//window.alert(errorMessage);
     	return throwError(errorMessage);
       //return {success:false,message:errorMessage};
  	}
}
