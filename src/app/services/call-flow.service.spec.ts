import { TestBed } from '@angular/core/testing';

import { CallFlowService } from './call-flow.service';

describe('CallFlowService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CallFlowService = TestBed.get(CallFlowService);
    expect(service).toBeTruthy();
  });
});
