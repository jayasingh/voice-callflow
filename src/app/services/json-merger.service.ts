import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JsonMergerService {
	//class constructor for loading dependencies
  	constructor() { }
  	//merger all json togather here.
  	do(flow, components, json){
		//push this value to flow array
		flow[json.identifier] = json;
		//initialize empty json object;
		var jsonObject = new Object();
		//check if all components flow has been recieved
	  	if(Object.keys(flow).length==components.length){
	  		//check if there is only one component
	  		if(Object.keys(flow).length==1){
	  			return json;
	  		}
			//merge all objects togather.
	      	for (var i = 1; i < Object.keys(flow).length; i++) {
		        var nextKey = Object.keys(flow)[i];
		        jsonObject = (i==1) ? flow['firstItem'] : jsonObject;
		        jsonObject = this.mergeHelper(jsonObject, flow[nextKey]);
	      	}
	  	}
		return jsonObject;	
  	}
  	//recursive function to search and repalace a string inside a json object
  	mergeHelper(first,second){
	    var keys = Object.keys(first);
	    for(let item of keys){
	      	if(first[item]==second['identifier']){
		        //remove identifier key as this is not required now.
		        delete second["identifier"];
		        //add second on this key.
		        first[item] = second;
		        break;
	      	}
	      	if(first[item] && typeof first[item] == 'object'){
	        	this.mergeHelper(first[item], second);
	      	}
	    }
	    return first;
  	}
}
