import { TestBed } from '@angular/core/testing';

import { ComponentDeclarationService } from './component-declaration.service';

describe('ComponentDeclarationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComponentDeclarationService = TestBed.get(ComponentDeclarationService);
    expect(service).toBeTruthy();
  });
});
