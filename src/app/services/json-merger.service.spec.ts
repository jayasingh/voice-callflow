import { TestBed } from '@angular/core/testing';

import { JsonMergerService } from './json-merger.service';

describe('JsonMergerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JsonMergerService = TestBed.get(JsonMergerService);
    expect(service).toBeTruthy();
  });
});
