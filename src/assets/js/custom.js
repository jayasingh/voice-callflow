function allowDrop(ev) {
	/* The default handling is to not allow dropping elements. */
	/* Here we allow it by preventing the default behaviour. */
	ev.preventDefault();
}

function dragMe(ev) {
	/* Here is specified what should be dragged. */
	/* This data will be dropped at the place where the mouse button is released */
	/* Here, we want to drag the element itself, so we set it's ID. */
	ev.dataTransfer.setData("text/html", ev.target.id);
}

function dropMe(ev, newId) {
  /* The default handling is not to process a drop action and hand it to the next 
    higher html element in your DOM. */
  /* Here, we prevent the default behaviour in order to process the event within 
    this handler and to stop further propagation of the event. */
  ev.preventDefault();
  /* In the drag event, we set the *variable* (it is not a variable name but a 
    format, please check the reference!) "text/html", now we read it out */
  var data=ev.dataTransfer.getData("text/html");
  //check if target div is not empty then drop
  if (ev.target.hasChildNodes()){ 
    return false; 
  }
  else{
    /* As we put the ID of the source element into this variable, we can now use 
       this ID to manipulate the dragged element as we wish. */
  	var nodeCopy = document.getElementById(data).cloneNode(true);
  	nodeCopy.id = newId; //"newId-"+Math.floor(Math.random() * 100) + 1; /* We cannot use the same ID */
    //make draggable false;
    nodeCopy.draggable = false;
    //add cross button to remove this object.
    var deleteNode = createDeleteNode();
    //append this to the node 
    nodeCopy.appendChild(deleteNode);
    //add node to target item
  	ev.target.appendChild(nodeCopy);
    //return true.
    return true;
  }
}

function createDeleteNode(){
  //create an span node.
  var deleteNode = document.createElement("span"); 
  //add a classname to this node
  deleteNode.className = "remove-me";
  //create a text node to this element.
  var deleteTextNode = document.createTextNode("x");
  //append this text node to deleteNode.
  deleteNode.appendChild(deleteTextNode);
  //add onclick functionality for this node.
  deleteNode.onclick = function () {
    //get parent element id
    var deleteComponent = document.getElementById(this.parentElement.id);
    //make a manual click on this id.
    deleteComponent.click();
    //remove its parent element, as delete clicked.
    this.parentElement.remove();
  };
  //return this node.
  return deleteNode;
}

function makeId(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}